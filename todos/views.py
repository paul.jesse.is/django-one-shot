from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import ListForm, ItemForm
# Create your views here.


def todo_lists_list(request):
    lists = TodoList.objects.all()
    context = {
        "todo_lists_list": lists
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "list_object": list
    }
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_lists_list")
    else:
        form = ListForm()
    context = {
        "list_form": form,
    }
    return render(request, "todos/create_list.html", context)


def create_todo_item(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm()
    context = {
        "item_form": form,
    }
    return render(request, "todos/create_item.html", context)


def edit_todo_list(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = ListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = ListForm(instance=list)
    context = {
        "list_object": list,
        "list_form": form,
    }
    return render(request, "todos/edit.html", context)


def edit_todo_item(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm(instance=item)
    context = {
        "item_object": item,
        "item_form": form,
    }
    return render(request, "todos/edit_item.html", context)


def delete_todo_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_lists_list")

    return render(request, "todos/delete.html")
